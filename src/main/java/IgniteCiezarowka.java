import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class IgniteCiezarowka {
    private Ignite ignite;

    public IgniteCiezarowka(Ignite ignite)
    {
        this.ignite = ignite;
    }
    public boolean save(Ciezarowka ciezarowka) throws UnknownHostException {
        final IgniteAtomicSequence seq = ignite.atomicSequence("seqCiezarowka", 0, true);
        CacheConfiguration cfg = new CacheConfiguration("ciezarowki");
        IgniteCache<Long, Ciezarowka> ciezarowki = ignite.getOrCreateCache(cfg);

        Long key1 = seq.incrementAndGet();
        try {
            ciezarowki.put(key1, ciezarowka);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public Map<Long, Ciezarowka> findAll(String garaz) throws UnknownHostException {
        CacheConfiguration cfg = new CacheConfiguration("ciezarowki");
        IgniteCache<Long, Ciezarowka> ciezarowki = ignite.getOrCreateCache(cfg);
        Map<Long, Ciezarowka> collect = new HashMap<Long, Ciezarowka>();
        for (Cache.Entry<Long, Ciezarowka> c : ciezarowki) {
            if(c.getValue().getGaraz().equals(garaz))
            {
                collect.put(c.getKey(),c.getValue());
            }
        }

        return collect;
    }

    public IgniteCache<Long, Ciezarowka> findAll() throws UnknownHostException {
        CacheConfiguration cfg = new CacheConfiguration("ciezarowki");
        IgniteCache<Long, Ciezarowka> ciezarowki = ignite.getOrCreateCache(cfg);

        return ciezarowki;
    }

    public boolean remove(Long id) throws UnknownHostException {
        CacheConfiguration cfg = new CacheConfiguration("ciezarowki");
        IgniteCache<Long, Ciezarowka> ciezarowki = ignite.getOrCreateCache(cfg);
        try {
            ciezarowki.remove(id);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Long id, Ciezarowka ciezarowka) throws UnknownHostException {
        CacheConfiguration cfg = new CacheConfiguration("ciezarowki");
        IgniteCache<Long, Ciezarowka> ciezarowki = ignite.getOrCreateCache(cfg);

        try {
            ciezarowki.put(id, ciezarowka);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
