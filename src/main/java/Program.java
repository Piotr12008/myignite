import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;

import javax.cache.Cache;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Program {
    public static String showMenu1() throws IOException {
        String znak = " ";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("GŁÓWNE MENU");
        System.out.println("1. Operacje po stronie klienta");
        System.out.println("2. Operacje po stronie serwera");
        System.out.println("q. Wyjdź");
        while(!znak.equals("q") && !znak.equals("1") && !znak.equals("2")) {
            znak = br.readLine();
        }
        return znak;
    }
    public static String showMenu2() throws IOException {
        String znak = " ";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("MENU");
        System.out.println("1. Kierowcy");
        System.out.println("2. Ciężarówki");
        System.out.println("3. Powrót");
        while(!znak.equals("3") && !znak.equals("1") && !znak.equals("2")) {
            znak = br.readLine();
        }
        return znak;
    }
    public static String showMenuKierowcy() throws IOException {
        String znak = " ";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("MENU KIEROWCY");
        System.out.println("1. Dodaj");
        System.out.println("2. Pokaż wszystkich");
        System.out.println("3. Edytuj kierowce");
        System.out.println("4. Wyszukiwanie kierowców");
        System.out.println("5. Powrót");
        while(!znak.equals("4") && !znak.equals("1") && !znak.equals("2") && !znak.equals("3") && !znak.equals("5")) {
            znak = br.readLine();
        }
        return znak;
    }
    public static String showMenuCiezarowka() throws IOException {
        String znak = " ";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("MENU CIEZAROWKI");
        System.out.println("1. Dodaj");
        System.out.println("2. Pokaż ciezarowki w garazu");
        System.out.println("3. Przypisz kierowce");
        System.out.println("4. Usun ciezarowke");
        System.out.println("5. Powrót");
        while(!znak.equals("4") && !znak.equals("1") && !znak.equals("2") && !znak.equals("3") && !znak.equals("5")) {
            znak = br.readLine();
        }
        return znak;
    }
    public static void main(String[] args) throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String znak = " ";
        while (true) {
            znak = showMenu1();
            if(znak.equals("2") || znak.equals("1")){
                IgniteConfig config = new IgniteConfig();
                Ignite ignite = null;
                if(znak.equals("2"))
                    ignite = config.startServer();
                if(znak.equals("1"))
                    ignite = config.newClient();
                IgniteKierowca mapKierowcaS = new IgniteKierowca(ignite);
                IgniteCiezarowka mapCiezarowkaS = new IgniteCiezarowka(ignite);
                while(true) {
                    znak = showMenu2();
                    if (znak.equals("1")) {
                        while(true) {
                            znak = showMenuKierowcy();
                            if (znak.equals("1")) {
                                System.out.println("Dodawanie kierowcy");
                                System.out.println("Podaj Imie");
                                String imie = in.readLine();
                                System.out.println("Podaj Nazwisko");
                                String nazwisko = in.readLine();
                                System.out.println("Podaj Miejscowosc");
                                String miejscowosc = in.readLine();
                                System.out.println("Podaj rok urodzenia");
                                Long rok = 0L;
                                try {
                                    rok = Long.parseLong(in.readLine());
                                }catch(Exception e){
                                    System.out.println(e.getMessage());
                                    System.out.println("Błąd. Rok musi być liczbą.");
                                }
                                Kierowca kierowca = new Kierowca(imie, nazwisko, miejscowosc,rok);
                                if (mapKierowcaS.save(kierowca) == true) {
                                    System.out.println("Dodano kierowcę");
                                } else {
                                    System.out.println("Błąd dodawania");
                                }
                            } else if (znak.equals("2")) {
                                IgniteCache<Long, Kierowca> kierowcy = mapKierowcaS.findAll();
                                System.out.println("Znalezieni kierowcy: ");
                                kierowcy.forEach((k) -> System.out.println((k.getKey() + " --- " + k.getValue().getImie() + " " + k.getValue().getNazwisko() + " " + k.getValue().getMiejscowosc() + " " + k.getValue().getRokUrodzenia())));
                            }
                            else if (znak.equals("3")){
                                IgniteCache<Long, Kierowca> kierowcy = mapKierowcaS.findAll();
                                kierowcy.forEach((k) -> System.out.println((k.getKey() + " --- " + k.getValue().getImie() + " " + k.getValue().getNazwisko() + " " + k.getValue().getMiejscowosc() + " " + k.getValue().getMiejscowosc())));
                                System.out.println("Podaj numer kierowcy do edycji: ");
                                Long ktory = Long.parseLong(in.readLine());
                                Kierowca kierowca = kierowcy.get(ktory);
                                if(kierowca != null) {
                                    System.out.println("Wpisuj dane które chcesz zmienić:");
                                    System.out.println("Podaj Imie");
                                    String imie = in.readLine();
                                    System.out.println("Podaj Nazwisko");
                                    String nazwisko = in.readLine();
                                    System.out.println("Podaj Miejscowość");
                                    String miejscowosc = in.readLine();
                                    System.out.println("Podaj rok urodzenia");
                                    String rokS = in.readLine();
                                    try {
                                        if (!rokS.equals(""))
                                            kierowca.setRokUrodzenia(Long.parseLong(rokS));
                                    }catch(Exception e)
                                    {
                                        System.out.println(e.getMessage());
                                        System.out.println("Błąd. Podano błędą datę");
                                    }
                                    if(!imie.equals(""))
                                        kierowca.setImie(imie);
                                    if(!nazwisko.equals(""))
                                        kierowca.setNazwisko(nazwisko);
                                    if(!miejscowosc.equals(""))
                                        kierowca.setMiejscowosc(miejscowosc);

                                    if (mapKierowcaS.update(ktory, kierowca))
                                        System.out.println("Zmodyfikowano kierowcę");
                                    else
                                        System.out.println("Błąd modyfikacji");
                                }else System.out.println("Podałeś zły numer");
                            }
                            else if (znak.equals("4")){
                                System.out.println("Podaj miejscowość: ");
                                String miejscowosc = in.readLine();
                                System.out.println("Podaj powyżej jakiego roku urodzenia mam szukać: ");
                                Long rok = Long.parseLong(in.readLine());
                                Collection<Kierowca> kierowcy = mapKierowcaS.findAllPredicate(miejscowosc,rok);
                                for(Kierowca k:kierowcy)
                                {
                                    System.out.println(k.getImie() + " " + k.getNazwisko() + " " + k.getMiejscowosc() + " " + k.getRokUrodzenia());
                                }
                            }
                            else if (znak.equals("5")) break;
                        }
                    }
                    else if(znak.equals("2"))
                    {
                        while(true) {
                            znak = showMenuCiezarowka();
                            if (znak.equals("1")) {
                                System.out.println("Dodawanie ciężarówki");
                                System.out.println("Podaj Marke");
                                String marka = in.readLine();
                                System.out.println("Podaj Model");
                                String model = in.readLine();
                                System.out.println("Podaj miejscowość w której znajduje się garaż");
                                String garaz = in.readLine();
                                System.out.println("Podaj numer rejestracyjny");
                                String nrRej = in.readLine();

                                Ciezarowka ciezarowka = new Ciezarowka(marka, model,nrRej,garaz);
                                if (mapCiezarowkaS.save(ciezarowka)) {
                                    System.out.println("Dodano ciężarówke");
                                } else {
                                    System.out.println("Błąd dodawania");
                                }
                            } else if (znak.equals("2")) {
                                System.out.println("Podaj miejscowość w której znajduje się garaż");
                                String garaz = in.readLine();
                                Map<Long, Ciezarowka> ciezarowki = mapCiezarowkaS.findAll(garaz);
                                System.out.println("Znalezione ciężarówki: ");
                                for (Map.Entry<Long, Ciezarowka> entry : ciezarowki.entrySet()) {
                                    Kierowca kierowca;
                                    if(entry.getValue().getKierowca() != 0)
                                    {
                                        kierowca = mapKierowcaS.find(entry.getValue().getKierowca());
                                        System.out.print((entry.getKey() + " --- " + entry.getValue().getMarka() + " " + entry.getValue().getModel() + " " + entry.getValue().getNrRejestr() + " "
                                                + entry.getValue().getGaraz()));
                                        System.out.println(" --- KIEROWCA: " + kierowca.getImie() + " " + kierowca.getNazwisko());
                                    }
                                    else
                                    {
                                        System.out.print((entry.getKey() + " --- " + entry.getValue().getMarka() + " " + entry.getValue().getModel() + " " + entry.getValue().getNrRejestr() + " "
                                                + entry.getValue().getGaraz()));
                                        System.out.println(" --- KIEROWCA: Brak");
                                    }
                                }
                            }
                            else if (znak.equals("3"))
                            {
                                System.out.println("Przypisanie kierowcy");
                                IgniteCache<Long, Ciezarowka> ciezarowki = mapCiezarowkaS.findAll();
                                IgniteCache<Long, Kierowca> kierowcy = mapKierowcaS.findAll();
                                System.out.println("Znalezione ciężarówki: ");
                                for (Cache.Entry<Long, Ciezarowka> entry : ciezarowki){
                                    Kierowca kierowca;
                                    System.out.print((entry.getKey() + " --- " + entry.getValue().getMarka() + " " + entry.getValue().getModel() + " " + entry.getValue().getNrRejestr() + " "
                                            + entry.getValue().getGaraz()));
                                    if(entry.getValue().getKierowca() != 0)
                                    {
                                        kierowca = kierowcy.get(entry.getValue().getKierowca());
                                        System.out.println(" --- KIEROWCA: " + kierowca.getImie() + " " + kierowca.getNazwisko());
                                    }
                                    else
                                    {
                                        System.out.println(" --- KIEROWCA: Brak");
                                    }
                                }
                                System.out.println("Podaj numer ciężarówki do przypisania kierowcy: ");
                                Long ktora = Long.parseLong(in.readLine());
                                Ciezarowka ciezarowka = ciezarowki.get(ktora);
                                Map<Long, Kierowca> kierowcyUnsig = mapKierowcaS.findAllUnsigned();
                                kierowcyUnsig.forEach((k, v) -> System.out.println((k + " --- " + v.getImie() + " " + v.getNazwisko() + " " + v.getMiejscowosc())));
                                if(kierowcyUnsig.isEmpty())
                                    System.out.println("Brak kierowców");
                                else {
                                    System.out.println("Podaj numer kierowcy do przypisania: ");
                                    Long ktory = Long.parseLong(in.readLine());
                                    ciezarowka.setKierowca(ktory);
                                    mapCiezarowkaS.update(ktora, ciezarowka);
                                }
                            }
                            else if (znak.equals("4")){
                                IgniteCache<Long, Ciezarowka> ciezarowki = mapCiezarowkaS.findAll();
                                IgniteCache<Long, Kierowca> kierowcy = mapKierowcaS.findAll();
                                System.out.println("Znalezione ciężarówki: ");
                                for (Cache.Entry<Long, Ciezarowka> entry : ciezarowki){
                                    Kierowca kierowca;
                                    System.out.print((entry.getKey() + " --- " + entry.getValue().getMarka() + " " + entry.getValue().getModel() + " " + entry.getValue().getNrRejestr() + " "
                                            + entry.getValue().getGaraz()));
                                    if(entry.getValue().getKierowca() != 0)
                                    {
                                        kierowca = kierowcy.get(entry.getValue().getKierowca());
                                        System.out.println(" --- KIEROWCA: " + kierowca.getImie() + " " + kierowca.getNazwisko());
                                    }
                                    else
                                    {
                                        System.out.println(" --- KIEROWCA: Brak");
                                    }
                                }
                                System.out.println("Podaj numer ciężarówki do usunięcia: ");
                                Long ktory = Long.parseLong(in.readLine());
                                if(mapCiezarowkaS.remove(ktory))
                                    System.out.println("Usunięto");
                                else
                                    System.out.println("Błąd usunięcia");
                            }
                            else if (znak.equals("5"))
                                break;
                        }
                    }
                    else if(znak.equals("3"))
                    {
                        Ignition.stop(false);
                        break;
                    }
                }
            }
            else if (znak.equals("q")) break;
        }
    }
}
