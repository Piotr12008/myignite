import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.io.IOException;

public class IgniteServer {
    public static void main(String[] args) throws IOException {
        Ignition.setClientMode(false);
        Ignite ignite = Ignition.start();
    }
}
