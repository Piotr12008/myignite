import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

public class IgniteKierowca {
    private Ignite ignite;

    public IgniteKierowca(Ignite ignite)
    {
        this.ignite = ignite;
    }

    public boolean save(Kierowca kierowca) throws UnknownHostException {
        final IgniteAtomicSequence seq = ignite.atomicSequence("seqKierowca", 0, true);
        CacheConfiguration cfg = new CacheConfiguration("kierowcy");
        IgniteCache<Long, Kierowca> kierowcy = ignite.getOrCreateCache(cfg);

        Long key1 = seq.incrementAndGet();
        try {
            kierowcy.put(key1, kierowca);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public IgniteCache<Long, Kierowca> findAll() throws UnknownHostException {
        CacheConfiguration cfg = new CacheConfiguration("kierowcy");
        IgniteCache<Long, Kierowca> kierowcy = ignite.getOrCreateCache(cfg);

        return kierowcy;
    }

    public Map<Long, Kierowca> findAllUnsigned() throws UnknownHostException {
        CacheConfiguration cfgK = new CacheConfiguration("kierowcy");
        IgniteCache<Long, Kierowca> kierowcy = ignite.getOrCreateCache(cfgK);
        CacheConfiguration cfgC = new CacheConfiguration("ciezarowki");
        IgniteCache<Long, Ciezarowka> ciezarowki = ignite.getOrCreateCache(cfgC);

        Map<Long, Kierowca> kierowcyUnsigned = new HashMap<Long, Kierowca>();

        for (Cache.Entry<Long, Kierowca> k : kierowcy) {
            boolean znaleziono = false;
            for (Cache.Entry<Long, Ciezarowka> c : ciezarowki) {
                if(c.getValue(). getKierowca().longValue() == k.getKey())
                {
                    znaleziono = true;
                    break;
                }
            }
            if(!znaleziono)
                kierowcyUnsigned.put(k.getKey(),k.getValue());
        }
        return kierowcyUnsigned;
    }

    public boolean update(Long id, Kierowca kierowca) throws UnknownHostException {
        CacheConfiguration cfgK = new CacheConfiguration("kierowcy");
        IgniteCache<Long, Kierowca> kierowcy = ignite.getOrCreateCache(cfgK);
        try {
            kierowcy.put(id, kierowca);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public Kierowca find(Long id) throws UnknownHostException {
        CacheConfiguration cfgK = new CacheConfiguration("kierowcy");
        IgniteCache<Long, Kierowca> kierowcy = ignite.getOrCreateCache(cfgK);
        Kierowca kierowca = kierowcy.get(id);

        return kierowca;
    }
    public Collection<Kierowca> findAllPredicate(String miejscowosc, Long rok) throws UnknownHostException {
        CacheConfiguration cfgK = new CacheConfiguration("kierowcy");
        IgniteCache<Long, Kierowca> kierowcy = ignite.getOrCreateCache(cfgK);

        //Predicate<> namePredicate = Predicates.equal( "miejscowosc", miejscowosc );
        //Predicate<?,??,?> birthyearPredicate = Predicates.greaterThan("rokUrodzenia", rok) ;

        //Collection<Kierowca> kierowcy2 = kierowcy.values(Predicates.and(namePredicate,birthyearPredicate));
        Collection<Kierowca> kierowcy2 = new LinkedList<Kierowca>();
        for (Cache.Entry<Long, Kierowca> k : kierowcy) {
            if(k.getValue().getMiejscowosc().equals(miejscowosc) && k.getValue().getRokUrodzenia() > rok) {
                kierowcy2.add(k.getValue());
            }
        }
        return kierowcy2;
    }
}
