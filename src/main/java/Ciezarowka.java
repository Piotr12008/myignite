import java.io.Serializable;

public class Ciezarowka implements Serializable {
    private static final long serialVersionUID = 1L;
    private String marka;
    private String model;
    private String nrRejestr;
    private String garaz;
    private Long kierowca;

    public Ciezarowka(String marka, String model, String nrRejestr, String garaz) {
        this.marka = marka;
        this.model = model;
        this.nrRejestr = nrRejestr;
        this.garaz = garaz;
        this.kierowca = 0L;
    }

    public Ciezarowka(String marka, String model, String nrRejestr, String garaz, Long kierowca) {
        this.marka = marka;
        this.model = model;
        this.nrRejestr = nrRejestr;
        this.garaz = garaz;
        this.kierowca = kierowca;
    }

    public String getMarka(){
        return marka;
    }

    public String getModel() {
        return model;
    }
    public String getNrRejestr() {

        return nrRejestr;
    }
    public String getGaraz() {

        return garaz;
    }
    public Long getKierowca() {

        return kierowca;
    }

    public void setMarka(String marka) {

        this.marka = marka;
    }

    public void setModel(String name) {

        this.model = model;
    }

    public void setNrRejestr(String nrRejestr) {

        this.nrRejestr = nrRejestr;
    }
    public void setGaraz(String garaz) {

        this.garaz = garaz;
    }
    public void setKierowca(Long kierowca) {

        this.kierowca = kierowca;
    }
}
