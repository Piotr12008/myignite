import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;

import java.net.UnknownHostException;

public class IgniteConfig {
    public static Ignite startServer() throws UnknownHostException {
        Ignition.setClientMode(false);
        Ignite ignite = Ignition.start();
        return ignite;
    }
    public static Ignite newClient(){
        Ignition.setClientMode(true);
        Ignite ignite = Ignition.start();
        return ignite;
    }
}
